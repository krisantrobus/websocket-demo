import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { IJourney } from '../components/interfaces/IJourney';
import { INotification } from '../components/interfaces/INotification';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private apiUrl: string = "http://localhost:8080";

  public journeys: IJourney[] = [];
  public notifications: Subject<INotification> = new Subject<INotification>();

  constructor(private http: HttpClient) { }

  public getJourneys = () => {
    return this.http.get<IJourney[]>(`${this.apiUrl}/journeys`);
  }

  public getNotifications = () => {
    return this.notifications.asObservable();
  }
}
