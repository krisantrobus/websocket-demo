import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { IJourney } from '../components/interfaces/IJourney';
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { INotification } from '../components/interfaces/INotification';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app-state';
import { AppActions } from '../store/app-action-types';

enum messageDestinations {
    journeys = "/journeys",
    notifications = "/notifications",
    journey = "/journey"
}

interface WsClients {
    subscription?: Subscription,
    stompClient: any
}

export enum WebsocketSubsriptions {
    JOURNEYS_ALL = "JOURNEYS_ALL",
    JOURNEY_ID = "JOURNEY_ID",
    NOTIFICATIONS = "NOTIFICATIONS"
}

@Injectable()
export class WebSocketAPI {
    private webSocketEndPoint: string = 'http://localhost:8080/ws';
    private ws: any;

    private wsJourneys: WsClients;
    private wsJourney: WsClients;
    private wsNotifications: WsClients;

    constructor(private ds: DataService, private store: Store<AppState>) { }

    public connectJourneys() {
        this.wsJourneys = {
            stompClient: Stomp.over(new SockJS(this.webSocketEndPoint))
        }
        this.wsJourneys.stompClient.debug = null;
        this.wsJourneys.stompClient.connect({}, () => {
            this.wsJourneys.subscription = this.wsJourneys.stompClient.subscribe(messageDestinations.journeys, (message) =>  {
                console.log(`incoming journey`);
                
                this.store.dispatch({
                    type: AppActions.JOURNEYS_UPDATE_ALL,
                    payload: <IJourney[]>JSON.parse(message.body)
                });
            });
        });
    };

    public connectNotifications() {
        this.wsNotifications = {
            stompClient: Stomp.over(new SockJS(this.webSocketEndPoint))
        }
        this.wsNotifications.stompClient.debug = null;
        this.wsNotifications.stompClient.connect({}, () => {
            this.wsNotifications.subscription = this.wsNotifications.stompClient.subscribe(messageDestinations.notifications, (message) => {
                this.store.dispatch({
                    type: AppActions.NOTIFICATION_ADD,
                    payload: <INotification>JSON.parse(message.body)
                });
            });
        });
    };

    public connectJourneyId(journeyId: string) {
        this.wsJourney = {
            stompClient: Stomp.over(new SockJS(this.webSocketEndPoint))
        }
        this.wsJourney.stompClient.debug = null;
        this.wsJourney.stompClient.connect({}, () => {
            this.wsJourney.subscription = this.wsJourney.stompClient.subscribe(`${messageDestinations.journeys}/${journeyId}`, (message) => {
                this.store.dispatch({
                    type: AppActions.JOURNEYS_UPDATE_ALL,
                    payload: <IJourney[]>JSON.parse(message.body)
                });
            });
        });
    };

    public disconnectAll() {
        for(let sub in WebsocketSubsriptions) {
            this.disconnect(sub);
        }
        this.store.dispatch({
            type: AppActions.NOTIFICATION_ADD,
            payload: "Disconnected"
        });
    }

    public disconnect = (WSSubscription : string) => {
        switch(WSSubscription){
            case WebsocketSubsriptions.JOURNEYS_ALL:
                this.wsJourneys.stompClient && this.wsJourneys.stompClient.disconnect();
                this.wsJourneys.subscription && this.wsJourneys.subscription.unsubscribe();
                break;
            case WebsocketSubsriptions.JOURNEY_ID:
                this.wsJourney.stompClient && this.wsJourney.stompClient.disconnect();
                this.wsJourney.subscription && this.wsJourney.subscription.unsubscribe();
                break
            case WebsocketSubsriptions.NOTIFICATIONS:
                this.wsNotifications.stompClient && this.wsNotifications.stompClient.disconnect();
                this.wsNotifications.subscription && this.wsNotifications.subscription.unsubscribe();
                break;
            default:
                console.log("unknown subscription");
        }
    }

    /**
     * Send message to sever via web socket
     * @param {*} message 
     */
    public sendJourney(message) {
        console.log("calling logout api via web socket");
        this.wsJourneys.stompClient.send("/app/journeys", {}, JSON.stringify(message));
    }
}