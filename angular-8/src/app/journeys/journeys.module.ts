import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JourneysRoutingModule } from './journeys-routing.module';
import { JourneysTableComponent } from '../components/journeys-table/journeys-table.component';
import { JourneysListComponent } from '../pages/journeys/journeys-list/journeys-list.component';
import { JourneysComponent } from '../pages/journeys/journeys.component';
import { JourneyDetailComponent } from '../pages/journeys/journey-detail/journey-detail.component';


@NgModule({
  declarations: [JourneysTableComponent, JourneysListComponent, JourneysComponent, JourneyDetailComponent],
  imports: [
    CommonModule,
    JourneysRoutingModule
  ]
})
export class JourneysModule { }
