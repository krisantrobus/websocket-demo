import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JourneysTableComponent } from '../components/journeys-table/journeys-table.component';
import { JourneysComponent } from '../pages/journeys/journeys.component';
import { JourneysListComponent } from '../pages/journeys/journeys-list/journeys-list.component';
import { JourneyDetailComponent } from '../pages/journeys/journey-detail/journey-detail.component';


const routes: Routes = [
  {
    path: '', component: JourneysComponent, children: [
      { path: 'all-journeys', component: JourneysListComponent },
      { path: '', redirectTo: '/journeys/all-journeys', pathMatch: "full" },
      { path: 'details/:journeyId', component: JourneyDetailComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JourneysRoutingModule { }
