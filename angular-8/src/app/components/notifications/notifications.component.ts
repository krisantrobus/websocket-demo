import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs';
import { INotification } from '../interfaces/INotification';
import { DataService } from 'src/app/services/data.service';
import { IconDefinition, faTimes } from '@fortawesome/free-solid-svg-icons';
import { AppState } from 'src/app/store/app-state';
import { Store } from '@ngrx/store';
import { getNotifications } from 'src/app/store/app-selector';
import { AppActions } from 'src/app/store/app-action-types';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  constructor(private ds: DataService, private store: Store<AppState>) { }

  public faCross: IconDefinition = faTimes;
  public notifications: Observable<INotification[]>;

  ngOnInit() {
    this.notifications = this.store.select(getNotifications);
  }

  public removeNotification = (notifcation : INotification) => {
    this.store.dispatch({
      type: AppActions.NOTIFICATION_REMOVE,
      payload: notifcation
    });
  }
}
