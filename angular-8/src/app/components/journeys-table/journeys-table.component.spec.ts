import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JourneysTableComponent } from './journeys-table.component';

describe('JourneysTableComponent', () => {
  let component: JourneysTableComponent;
  let fixture: ComponentFixture<JourneysTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JourneysTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JourneysTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
