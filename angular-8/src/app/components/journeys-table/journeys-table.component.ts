import { Component, OnInit, OnDestroy } from '@angular/core';
import { IJourney } from '../interfaces/IJourney';
import { DataService } from 'src/app/services/data.service';
import { Subscription, Observable } from 'rxjs';
import { WebSocketAPI } from 'src/app/services/websocket';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app-state';
import { getJourneys } from 'src/app/store/app-selector';
import { Router } from '@angular/router';
import { AppActions } from 'src/app/store/app-action-types';

@Component({
  selector: 'app-journeys-table',
  templateUrl: './journeys-table.component.html',
  styleUrls: ['./journeys-table.component.scss']
})
export class JourneysTableComponent implements OnInit {

  constructor(public ds: DataService, private ws: WebSocketAPI, private store: Store<AppState>, private router: Router) { }

  private journeys: Observable<IJourney[]>;

  ngOnInit(): void {
    this.journeys = this.store.select(getJourneys);
  }

  public sendJourney = (journey: IJourney) => {
    this.store.dispatch({
      type: AppActions.JOURNEY_SELECT,
      payload: journey
    })
    this.router.navigate([`/journeys/details/${journey.operator}`]);

    // this.ws.sendJourney(journey);
  }
}
