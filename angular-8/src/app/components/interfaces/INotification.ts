export interface INotification {
    header: string;
    time: Date;
    message: string;
}