export interface IJourney {
    status: string,
    operator:  string,
    normalPrice: number,
    type: string,
    journeyLegs: [
        {
            from:string,
            to: string,
            startDateTime: Date,
            endDateTime: Date,
            mode: string
        }
    ]
}