import { IJourney } from '../components/interfaces/IJourney';
import { INotification } from '../components/interfaces/INotification';

export interface AppState {
    app: AppFeatureState
}

export interface AppFeatureState {
    journeys: IJourney[];
    selectedjourney: IJourney
    notifications: INotification[]
    loading: boolean;
}

export const initalAppState: AppFeatureState = {
    journeys: [],
    notifications: [],
    loading: false,
    selectedjourney: null
}