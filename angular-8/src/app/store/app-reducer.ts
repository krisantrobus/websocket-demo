import { initalAppState, AppFeatureState } from './app-state';
import { AppActions } from './app-action-types';
import { IActionNgRX } from '../components/interfaces/IActionNgRX';

export function appReducer(state: AppFeatureState = initalAppState, action: IActionNgRX) {
    switch (action.type) {
        case AppActions.LOADING_TOGGLE:
            return { ...state, loading: !state.loading };
        case AppActions.JOURNEYS_UPDATE_ALL:
            return { ...state, journeys: action.payload };
        case AppActions.JOURNEY_SELECT:
            return { ...state, selectedjourney: action.payload}
        case AppActions.NOTIFICATION_ADD:
            return { ...state, notifications: [...state.notifications, action.payload] }
        case AppActions.NOTIFICATION_REMOVE: {
            return {
                ...state,
                notifications: state.notifications.filter(notification => notification != action.payload)
            }
        }
        default:
            return state;
    }
}