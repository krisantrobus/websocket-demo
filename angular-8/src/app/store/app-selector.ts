import { createSelector } from "@ngrx/store";
import { AppState } from './app-state';

const state = (state: AppState) => state;

export const getLoading = createSelector(
    state,
    (state: AppState) => state.app.loading
);

export const getJourneys = createSelector(
    state,
    (state: AppState) => state.app.journeys
);

export const getNotifications = createSelector(
    state,
    (state: AppState) => state.app.notifications
);

export const getSelectedJourney = createSelector(
    state,
    (state: AppState) => state.app.selectedjourney
);