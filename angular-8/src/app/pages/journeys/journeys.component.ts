import { Component, OnInit, OnDestroy } from '@angular/core';
import { WebSocketAPI, WebsocketSubsriptions } from 'src/app/services/websocket';

@Component({
  selector: 'app-journeys',
  templateUrl: './journeys.component.html',
  styleUrls: ['./journeys.component.scss']
})
export class JourneysComponent implements OnInit, OnDestroy {

  constructor(private ws: WebSocketAPI) { }

  ngOnInit() {
    this.ws.connectJourneys();
  }

  ngOnDestroy(){
    this.ws.disconnect(WebsocketSubsriptions.JOURNEYS_ALL)
  }

}
