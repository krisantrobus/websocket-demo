import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app-state';
import { Subscription } from 'rxjs';
import { IJourney } from 'src/app/components/interfaces/IJourney';
import { getSelectedJourney } from 'src/app/store/app-selector';
import { ActivatedRoute } from '@angular/router';
import { WebSocketAPI, WebsocketSubsriptions } from 'src/app/services/websocket';

@Component({
  selector: 'app-journey-detail',
  templateUrl: './journey-detail.component.html',
  styleUrls: ['./journey-detail.component.scss']
})
export class JourneyDetailComponent implements OnInit, OnDestroy {

  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute, private ws: WebSocketAPI) { }

  public journey: IJourney;
  private sub: Subscription;

  ngOnInit() {
    this.sub = this.store.select(getSelectedJourney).subscribe(journey => {
      if(journey){
        this.journey = journey;
      } else {
        this.getSelectedJourneyfromAPI();
      }
    })
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
    this.ws.disconnect(WebsocketSubsriptions.JOURNEY_ID);
  }

  private getSelectedJourneyfromAPI = () => {
    const journeyId = this.activatedRoute.snapshot.params['journeyId']

    console.log(journeyId);
    
  }

}
