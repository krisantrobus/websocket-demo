import { Component, OnInit, OnDestroy } from '@angular/core';
import { IJourney } from './components/interfaces/IJourney';
import { WebSocketAPI } from './services/websocket';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'weboscket-demo';

  constructor(private ws: WebSocketAPI){}

  ngOnInit() {
    this.ws.connectNotifications();
  }

  ngOnDestroy() {
    this.ws.disconnectAll();
  }
}
