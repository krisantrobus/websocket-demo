/*
 * Copyright 2019 Applied Card Technologies Ltd
 */
package act.websocketdemo.websocketdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import act.websocketdemo.websocketdemo.model.Notification;

/**
 * @author kristian.antrobus
 */

@RestController
public class NotificationController
{
    @Autowired
    private SimpMessagingTemplate template;

    @SendTo( "/notifications" )
    @MessageMapping( "/notifications" )
    @RequestMapping( value = "/notifications", method = RequestMethod.POST )
    public String postJourney( @RequestBody Notification notification )
    {
        template.convertAndSend( "/notifications", notification );
        return "Notification successfully Sent";
    }
}
