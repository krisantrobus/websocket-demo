/*
 * Copyright 2019 Applied Card Technologies Ltd
 */
package act.websocketdemo.websocketdemo.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import act.websocketdemo.websocketdemo.model.Journey;
import act.websocketdemo.websocketdemo.model.JourneyLeg;

/**
 * @author kristian.antrobus
 */

@RestController
public class JourneyController
{
    private static final String START_DATE_JOURNEY = "2019-11-06 13:20:00";
    private static final String END_DATE_JOURNEY = "2019-11-06 13:55:00";
    private final AtomicLong counter = new AtomicLong();
    private List<Journey> journeys = new ArrayList<Journey>();

    @Autowired
    private SimpMessagingTemplate template;

    @SendTo( "/journeys" )
    @RequestMapping( value = "/journeys", method = RequestMethod.GET )
    public List<Journey> getJourneysCall()
    {
        if ( journeys.size() > 0 )
        {
            return journeys;
        }
        return getJourneys();
    }

    @SendTo( "/journeys" )
    @MessageMapping( "/journeys" )
    @RequestMapping( value = "/journeys", method = RequestMethod.POST )
    public List<Journey> postJourney( @RequestBody Journey journey )
    {
        journeys.add( journey );
        template.convertAndSend( "/journeys", journeys );
        return journeys;
    }


    private List<Journey> getJourneys()
    {
        DateFormat format = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );

        JourneyLeg journeyLeg = null;
        try
        {
            journeyLeg = new JourneyLeg()
                    .startDateTime( format.parse( START_DATE_JOURNEY ) )
                    .endDateTime( format.parse( END_DATE_JOURNEY ) )
                    .from( "Cardiff Station" )
                    .to( "Cardiff Stadium" )
                    .mode( "BUS" );
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }

        Journey journey = new Journey()
                .journeyLegs( Collections.singletonList( journeyLeg ) )
                .normalPrice( 2.78f )
                .operator( "Cardiff Bus" )
                .type( "CFERGD" )
                .status( "COMPLETE" );

        Journey journey2 = new Journey()
                .journeyLegs( Collections.singletonList( journeyLeg ) )
                .normalPrice( 0.88f )
                .operator( "Next Bike" )
                .type( "CFERGD" )
                .status( "PENDING" );

        journeys.add( journey );
        journeys.add( journey2 );
        return journeys;
    }
}
