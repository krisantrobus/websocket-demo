/*
 * Copyright 2019 Applied Card Technologies Ltd
 */
package act.websocketdemo.websocketdemo.model;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author kristian.antrobus
 */
public class JourneyLeg
{
    @JsonFormat( pattern = "yyyy-MM-dd hh:mm:ss" )
    private Date startDateTime;
    @JsonFormat( pattern = "yyyy-MM-dd hh:mm:ss" )
    private Date endDateTime;
    private String from;
    private String to;
    private String mode;

    public JourneyLeg()
    {
    }

    public JourneyLeg( final Date startDateTime, final Date endDateTime, final String from, final String to,
                       final String mode )
    {
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.from = from;
        this.to = to;
        this.mode = mode;
    }

    public JourneyLeg startDateTime( final Date startDateTime )
    {
        this.startDateTime = startDateTime;
        return this;
    }

    public JourneyLeg endDateTime( final Date endDateTime )
    {
        this.endDateTime = endDateTime;
        return this;
    }

    public JourneyLeg from( final String from )
    {
        this.from = from;
        return this;
    }

    public JourneyLeg to( final String to )
    {
        this.to = to;
        return this;
    }

    public JourneyLeg mode( final String mode )
    {
        this.mode = mode;
        return this;
    }


    /**
     * @return The startDateTime.
     */
    public Date getStartDateTime()
    {
        return startDateTime;
    }

    /**
     * @param startDateTime The startDateTime to set.
     */
    public void setStartDateTime( final Date startDateTime )
    {
        this.startDateTime = startDateTime;
    }

    /**
     * @return The endDateTime.
     */
    public Date getEndDateTime()
    {
        return endDateTime;
    }

    /**
     * @param endDateTime The endDateTime to set.
     */
    public void setEndDateTime( final Date endDateTime )
    {
        this.endDateTime = endDateTime;
    }

    /**
     * @return The from.
     */
    public String getFrom()
    {
        return from;
    }

    /**
     * @param from The from to set.
     */
    public void setFrom( final String from )
    {
        this.from = from;
    }

    /**
     * @return The to.
     */
    public String getTo()
    {
        return to;
    }

    /**
     * @param to The to to set.
     */
    public void setTo( final String to )
    {
        this.to = to;
    }

    /**
     * @return The mode.
     */
    public String getMode()
    {
        return mode;
    }

    /**
     * @param mode The mode to set.
     */
    public void setMode( final String mode )
    {
        this.mode = mode;
    }


}
