/*
 * Copyright 2019 Applied Card Technologies Ltd
 */
package act.websocketdemo.websocketdemo.model;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author kristian.antrobus
 */
public class Notification
{
    private String header;
    @JsonFormat( pattern = "yyyy-MM-dd hh:mm:ss" )
    private Date time;
    private String message;

    public Notification()
    {
    }

    public Notification( final String header, final Date time, final String message )
    {
        this.header = header;
        this.time = time;
        this.message = message;
    }

    public Notification header( final String header )
    {
        this.header = header;
        return this;
    }

    public Notification message( final String message )
    {
        this.message = message;
        return this;
    }

    public Notification time( final Date time )
    {
        this.time = time;
        return this;
    }

    /**
     * @return The header.
     */
    public String getHeader()
    {
        return header;
    }

    /**
     * @param header The header to set.
     */
    public void setHeader( final String header )
    {
        this.header = header;
    }

    /**
     * @return The time.
     */
    public Date getTime()
    {
        return time;
    }

    /**
     * @param time The time to set.
     */
    public void setTime( final Date time )
    {
        this.time = time;
    }

    /**
     * @return The message.
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * @param message The message to set.
     */
    public void setMessage( final String message )
    {
        this.message = message;
    }
}

