/*
 * Copyright 2019 Applied Card Technologies Ltd
 */
package act.websocketdemo.websocketdemo.model;

import java.util.List;

/**
 * @author kristian.antrobus
 */
public class Journey
{
    private String status;
    private String type;
    private float normalPrice;
    private String operator;
    private List<JourneyLeg> journeyLegs;

    public Journey()
    {
    }

    public Journey( final String status, final String type, final float normalPrice, final String operator,
                    final List<JourneyLeg> journeyLegs )
    {
        this.status = status;
        this.type = type;
        this.normalPrice = normalPrice;
        this.operator = operator;
        this.journeyLegs = journeyLegs;
    }

    public Journey status( final String status )
    {
        this.status = status;
        return this;
    }

    public Journey type( final String type )
    {
        this.type = type;
        return this;
    }

    public Journey operator( final String operator )
    {
        this.operator = operator;
        return this;
    }

    public Journey normalPrice( final float normalPrice )
    {
        this.normalPrice = normalPrice;
        return this;
    }

    public Journey journeyLegs( final List<JourneyLeg> journeyLegs )
    {
        this.journeyLegs = journeyLegs;
        return this;
    }

    /**
     * @return The status.
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * @param status The status to set.
     */
    public void setStatus( final String status )
    {
        this.status = status;
    }

    /**
     * @return The type.
     */
    public String getType()
    {
        return type;
    }

    /**
     * @param type The type to set.
     */
    public void setType( final String type )
    {
        this.type = type;
    }

    /**
     * @return The normalPrice.
     */
    public float getNormalPrice()
    {
        return normalPrice;
    }

    /**
     * @param normalPrice The normalPrice to set.
     */
    public void setNormalPrice( final float normalPrice )
    {
        this.normalPrice = normalPrice;
    }

    /**
     * @return The operator.
     */
    public String getOperator()
    {
        return operator;
    }

    /**
     * @param operator The operator to set.
     */
    public void setOperator( final String operator )
    {
        this.operator = operator;
    }

    /**
     * @return The journeyLegs.
     */
    public List<JourneyLeg> getJourneyLegs()
    {
        return journeyLegs;
    }

    /**
     * @param journeyLegs The journeyLegs to set.
     */
    public void setJourneyLegs( final List<JourneyLeg> journeyLegs )
    {
        this.journeyLegs = journeyLegs;
    }

    @Override
    public String toString()
    {
        return String.format( "Operator: %s, status: %s, StartTime: %s, from: %s",
                this.operator,
                this.status,
                this.getJourneyLegs().get( 0 ).getStartDateTime(),
                this.getJourneyLegs().get( 0 ).getFrom() );
    }
}
